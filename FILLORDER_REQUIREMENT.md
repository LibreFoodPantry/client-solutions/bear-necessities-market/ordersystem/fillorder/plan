# Requirement #

## Title: Fill An Order

## Pre-Checklist

The "View Orders to be filled" requirement is related to this requirement.

## Stories

Story 1: As an administrator or staff member I want to be able to fill in order so that the order can be processed.

Story 2: As an administrator or staff member I want to be able to see the dietary restrictions of an order so that the order can be filled while adhering to the guest's needs.

Story 3: As an administrator or staff member I want to be able to replace a food item with a substitute food item if that item is out of stock so that the guest can still recieve their order even if one item is out of stock.

Story 4: As an administrator or staff member I want to leave a note while filling an order so that the guest will be informed of replaced items on the order.

Story 5: As an administrator or staff member I want to be able to sign off on an order after I fill an order so that the system can keep track of who filled which order.

## Ready Checklist

| Story 1: As an administrator or staff member I want to be able to fill in order so that the order can be processed.  | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

| Story 2: As an administrator or staff member I want to be able to see the dietary restrictions of an order so that the order can be filled while adhering to the guest's needs.  | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

| Story 3: As an administrator or staff member I want to be able to replace a food item with a substitute food item if that item is out of stock so that the guest can still recieve their order even if one item is out of stock. | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | Yes |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

| Story 4: As an administrator or staff member I want to leave a note while filling an order so that the guest will be informed of replaced items on the order.  | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

| Story 5: As an administrator or staff member I want to be able to sign off on an order after I fill an order so that the system can keep track of who filled which order. | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | Yes |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

## Diagrams
Current Fill Order Page
![Fill Order Page](https://i.imgur.com/GzzGrkA.png)


## Acceptance Criteria

### Scenario : Staff recieves an with dietary restrictions

Given : The staff chooses to Fill an Order with dietary restrictions

When : The staff fills out the order

Then : The order is displayed alongside the dietary restrictions.

### Scenario : Staff tries to fill an order with an item out of stock

Given : The staff chooses to Fill an Order

When : The staff fills out the order item that is out of stock

Then : The out of stock item checkbox is not selectable.

## Related Issues

- Parent: [https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/fillorder/fullstack/-/issues/1](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/fillorder/fullstack/-/issues/1)
  - This link is the plan for the future for Fillorder so it is the major issue to follow.
- Depends-on: [https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/fillorder/Frontend/uploads/3a48a4fe7a37d8e86e667bbee6446b90/OrderListScreenUpdated.png](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/fillorder/Frontend/uploads/3a48a4fe7a37d8e86e667bbee6446b90/OrderListScreenUpdated.png)
  - This issue presents the page for the &quot;List of Orders&quot; for the Fillorder module making it the primary issue related to the module.

## Estimate

Story 1: 
Estimate: 13

Story 2:
Estimate: 3

Story 3:
Estimate: 5

Story 4: 
Estimate: 1

Story 5: 
Estimate: 1


# Requirement #
## Title: View Orders To Be Filled
## Pre-Checklist

The "Fill An Order" requirement is related to this requirement.

## Stories

Story 1: As a Staff or Administrator I want to be able to view the orders that need to be filled so that I can keep the food pantry guests satisfied.

Story 2: As a Staff or Administrator I want to prohibit volunteers from filling out their own orders so that Volunteers do not take advantage of the system.

Story 3: As a Staff or Administrator I want to be able to see the date, ID, and number of items for each order so that I know which orders to fill first.

## Ready Checklist

| Story 1: As a Staff or Administrator I want to be able to view the orders that need to be filled so that I can keep the food pantry guests satisfied. | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

| Story 2: As a Staff or Administrator I want to prohibit volunteers from filling out their own orders so that Volunteers do not take advantage of the system.  | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

| Story 3: As a Staff or Administrator I want to be able to see the date, ID, and number of items for each order so that I know which orders to fill first. | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | No |
| The roles that benefit from this issue are labeled (e.g., role:\*). | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

## Diagrams

![Fill Order Page](https://i.imgur.com/aIRzfXO.png)

## Acceptance Criteria

### Scenario : Volunteer filling order

Given : Orders are displayed on the order page

When : The volunteer chooses to Fill an Order

Then : They will be brought to the Fill Order page.

### Scenario : Non conflicting filling

Given : A volunteer decides to fill an order

When : The volunteer is filling that order

Then : Any other volunteer will not be able to fill that order.


## Related Issues

[fillorder#13](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/fillorder/Frontend/-/issues/13)

## Estimate

Story 1: 
Estimate: 8

Story 2: 
Estimate: 3

Story 3: 
Estimate: 3